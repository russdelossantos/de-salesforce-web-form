let demodal_styles = "<STYLES_BASE64>";
let neverbounce_code = "<NEVERBOUNCE_CODE>";
let lead_modal_element = `  <div class="modal-container" id="modal_container">
<div class="modal">
  <button type="button" class="close-button" id="close">&times;</button>
  <form action="https://test.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8" method="POST" class="form"
    id="main_form">

    <input type=hidden name='captcha_settings'
      value='{"keyname":"DE_Russ_Test_v2","fallback":"true","orgId":"00D7j000000H0te","ts":""}'>
    <input type=hidden name="oid" value="00D7j000000H0te">
    <input type=hidden name="retURL" value="https://designeverest.com/thank-you.html">

    <h3>DEBUG</h3>
    <span class="error-message" id="error-message"></span>
    <div class="row">
      <input id="first_name" class="col-50" maxlength="40" name="first_name" size="20" type="text"
        placeholder="Name" required />
      <input id="email" class="col-50" maxlength="80" name="email" size="20" type="text" placeholder="Email"
        required />
    </div>


    <div class="row">
      <input id="phone" class="col-50" maxlength="40" name="phone" size="20" type="text" placeholder="Phone Number"
        required />
      <input id="zip" class="col-50" maxlength="20" name="zip" size="20" type="text" placeholder="Zip Code"
        required />
    </div>

    <div class="row">
      <textarea id="00N7j000001mtz9" class="col-100" name="00N7j000001mtz9" type="text" wrap="soft"
        placeholder="What are you looking for in your project?" rows="3" required></textarea>
    </div>

    <input id="00N7j000001msvA" maxlength="1023" name="00N7j000001msvA" size="20" type="hidden"/>
    <div class="row">
      <div class="g-recaptcha col-100" data-sitekey="6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI"></div>
    </div>
    <div class="row div-input-1col">
      <button id="button_submit" class="action-button div-input-1col-element"
        onclick="return validateForm();">Submit your query</button>
    </div>
</div>
</div>`


window.onload = function () {
    const lead = document.getElementById('lead-modal-button');
    const modal_container = document.getElementById('modal_container');
    const close = document.getElementById('close');
    const submit = document.getElementById('button_submit')

    lead.addEventListener('click', () => {
        modal_container.classList.add('show');
        modal_container.classList.remove('close')

    });

    close.addEventListener('click', () => {
        modal_container.classList.add('close');
        modal_container.classList.remove('show')
    })
}

function validateForm() {
    const name = document.getElementById('first_name');
    const email = document.getElementById('email');
    const zip = document.getElementById('zip');
    const phone = document.getElementById('phone');
    const description = document.getElementById('00N7j000001mtz9');
    const tracking = document.getElementById('00N7j000001msvA')
    const error = document.getElementById('error-message');

    // remove previous error styling
    name.classList.remove('invalid');
    email.classList.remove('invalid');
    zip.classList.remove('invalid');
    phone.classList.remove('invalid');
    description.classList.remove('invalid');
    error.classList.remove('show-error')

    if (!name.value) {
        name.classList.add('invalid');
        setTimeout(function () {
            name.classList.remove('invalid');
        }, 5000);
        error.classList.add('show-error');
        error.innerHTML = "Name is required.";
        return false
    }

    if (!email.value) {
        email.classList.add('invalid');
        setTimeout(function () {
            email.classList.remove('invalid');
        }, 5000);
        error.classList.add('show-error');
        error.innerHTML = "Email is required.";
        return false;
    }

    if (!validateEmail(email.value)) {
        email.classList.add('invalid');
        setTimeout(function () {
            email.classList.remove('invalid');
        }, 5000);
        error.classList.add('show-error');
        error.innerHTML = "Valid email is required.";
        return false;
    }

    if (!zip.value) {
        zip.classList.add('invalid');
        setTimeout(function () {
            zip.classList.remove('invalid');
        }, 5000);
        error.classList.add('show-error');
        error.innerHTML = "Project ZIP code is required.";
        return false;
    }

    if (!phone.value) {
        phone.classList.add('invalid');
        setTimeout(function () {
            phone.classList.remove('invalid');
        }, 5000);
        error.classList.add('show-error');
        error.innerHTML = "Phone number is required.";
        return false;
    }

    if (!description.value) {
        description.classList.add('invalid');
        setTimeout(function () {
            description.classList.remove('invalid');
        }, 5000);
        error.classList.add('show-error');
        error.innerHTML = "Project description is required.";
        return false;
    }
    //testing sandbox Salesforce web to form
    /**
     $.ajax({
    url: `https://test.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8`,
    method: 'POST',
    data: { payload: JSON.stringify(form_data) },
    success: () => {
        deself._onFormSent(success);
    },
    error: () => {
        deself.showDialogScreen("error");
    },
    });

    const form = document.getElementById("main_form");
    form.submit();
    return true;
    */
    try {
        const tracking_data = getTracking();
        tracking.value = JSON.stringify(tracking_data);
        console.log(tracking.value)

    } catch (err) {
        console.log(err)
        return false;
    }
    const form = document.getElementById("main_form");
    form.submit();
    return true;

}

function validateEmail(email) {
    if (email) {
        let re =
            /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        return re.test(email.toLowerCase());
    }
    return false;
}

function getTracking() {
    return {
        tag: "",
        variant: "",
        custom: "",
        qs: window.location.search,
        location: window.location.href,
        original_qs: readStorageItem("original_qs") || "no_original_qs",
        referrer: readStorageItem("original_referrer") || "no_original_referrer",
        ga: getCookie("_ga") || "no_ga",
        gid: getCookie("_gid") || "no_gid",
        kvid: getCookie("kvid") || "no_kvid",
        gclids: getAllGclids() || "no_gclids",
        firm_name: "",

        first_enter_ts: readStorageItem("first_enter_ts") || 0,
        lead_ts: new Date().getTime(),

        dedata_rkt: getCookieJson("dedata_rkt") || "no_dedata_rkt"
    }
}

function str2b64(str) {
    return btoa(unescape(encodeURIComponent(str)));
}

function getCookieJson(name) {
    const data = getCookie(name);
    if (data === null || data === undefined) return null;
    return JSON.parse(b642str(data));
}

function getCookie(name) {
    let re = new RegExp(name + "=([^;]+)");
    let value = re.exec(document.cookie);
    return value != null ? unescape(value[1]) : null;
}

function b642str(str) {
    return decodeURIComponent(escape(atob(str)));
}

function getDataFromCookie() {
    const data = getCookieJson("dedata");
    return data || {};
}

function readStorageItem(name) {
    try {
        const data = getDataFromCookie();
        return data[name];
    } catch (e) {
        return null;
    }
}

function getCookieJson(name) {
    const data = getCookie(name);
    if (data === null || data === undefined) return null;
    return JSON.parse(b642str(data));
}

function getCookie(name) {
    let re = new RegExp(name + "=([^;]+)");
    let value = re.exec(document.cookie);
    return value != null ? unescape(value[1]) : null;
}


function getAllGclids() {
    const gclids = [];
    const cookies = getAllCookies();
    for (const name in cookies)
        if (name.match(/^gclid\d+/)) gclids.push(cookies[name]);
    return gclids;
}

function getAllCookies() {
    const cookies = {};

    if (document.cookie && document.cookie !== "") {
        const split = document.cookie.split(";");
        for (let i = 0; i < split.length; i++) {
            const name_value = split[i].split("=");
            name_value[0] = name_value[0].replace(/^ /, "");
            cookies[decodeURIComponent(name_value[0])] = decodeURIComponent(
                name_value[1]
            );
        }
    }

    return cookies;
}

function saveTracking() {
    let a = document.createElement("a");
    a.href = document.referrer;

    if (a.host !== "designeverest.com" && a.host !== "www.designeverest.com")
        saveStorageItemIfEmpty("original_referrer", document.referrer);

    saveStorageItemIfEmpty("original_qs", window.location.search);

    saveStorageItemIfEmpty("first_enter_ts", new Date().getTime());

    // Rakuten
    const rakutenSiteId = getUrlParam("siteID");
    if (rakutenSiteId) {
        setCookieJson(
            "dedata_rkt",
            {
                site_id: rakutenSiteId,
                enter_ts: new Date().getTime(),
            },
            2 * 365
        );
    }

    saveGclid();

    const demodalSessionId = getCookie("demodal_session_id");
    /**
    let form_data = createFormData();
    $.ajax({
        url: "https://app.designeverest.com/social/form/tracking",
        method: "POST",
        data: {
            demodal_session_id: demodalSessionId,
            form_data: JSON.stringify(form_data),
        },
    });
     */
}

function saveStorageItemIfEmpty(name, val) {
    try {
        const data = getDataFromCookie();
        if (data[name] === undefined || data[name] === null) {
            data[name] = val;
            saveDataToCookie(data);
        }
    } catch (e) { }
}

function getUrlParam(p) {
    const match = RegExp("[?&]" + p + "=([^&]*)").exec(window.location.search);
    return match && decodeURIComponent(match[1].replace(/\+/g, " "));
}

function saveDataToCookie(data) {
    setCookieJson("dedata", data);
}

function setCookieJson(name, data, days) {
    console.log(`Setting JSON cookie ${name} to`, data);
    setCookie(name, str2b64(JSON.stringify(data)), days);
}

function setCookie(name, value, days) {
    if (!days) days = 2 * 365;

    const date = new Date();
    date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);

    const domain = "localhost" // : "designeverest.com";

    const cookieExpires = `; expires=${date.toGMTString()}`;
    const cookieDomain = `; domain=${domain}`;

    const cookie =
        name + "=" + value + cookieExpires + cookieDomain + "; path=/";
    console.log(`Setting cookie (days: ${days})`, cookie);
    document.cookie = cookie;
}

function saveGclid() {
    const gclid = getUrlParam("gclid");
    if (!gclid) {
        console.log("No gclid");
        return "";
    }

    const gclsrc = getUrlParam("gclsrc");
    if (gclsrc && gclsrc.indexOf("aw") === -1) {
        console.log("gclsrc has 'aw', ignoring");
        return false;
    }

    const gclids = getAllGclids();
    console.log(`current gclids: ${gclids.join(", ")}`);
    if (gclids.some((x) => x === gclid)) {
        console.log("gclid already saved");
        return;
    }

    const cookies = getAllCookies();
    for (let i = 1; ; i++) {
        const name = `gclid${i}`;
        if (!cookies[name]) {
            console.log(`Adding gclid as ${name}`);
            setCookie(name, gclid, 90);
            break;
        }
    }
}

function addNeverBounceApi() {
    window._NBSettings = {
        apiKey: "public_0f16b3ce7c5261e0484628c42eb8bc3c",
        ajaxMode: true,
        displayPoweredBy: false,
    };

    const s = document.getElementsByTagName("script")[0];
    const b = document.createElement("script");
    b.type = "text/javascript";
    b.innerText = neverbounce_code;
    // b.async = true;
    // b.defer = true;
    // b.src = "https://cdn.neverbounce.com/widget/dist/NeverBounce.js";
    s.parentNode.insertBefore(b, s);
}
